<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <!-- Custom styles for this template -->
    <link href="/css/login.css" rel="stylesheet">
</head>
<body>
<div class="container">
    @include('flash')

    @yield('content')
</div>
@include('layouts/footer')
</body>
</html>