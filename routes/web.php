<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


//Route::middleware(['guest'])->group(function () {
//    Route::get('/login', function () {
//        return view('auth.login');
//    });

//    Route::post('/postlogin', 'RegistrationController@postLogin');
//    Route::post('/postregister', 'RegistrationController@postregister');
//});


//Route::post('/postregister', 'RegistrationController@postregister');
//https://packagist.org/packages/jrean/laravel-user-verification
//Route::group(['middleware' => 'web'], function () {
//    Route::get('email-verification/error', 'RegisterController@getVerificationError')->name('email-verification.error');
//    Route::get('email-verification/check/{token}', 'RegisterController@getVerification')->name('email-verification.check');
//});

//Route::get('/register', 'RegistrationController@register');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
