<?php

namespace App\Http\Controllers;

use App\User;
//use App\Mailers\AppMailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Traits\RedirectsUsers;
use Jrean\UserVerification\Traits\UserVerification;

class RegistrationController extends Controller
{
//    use VerifiesUsers, RedirectsUsers, UserVerification;
    use UserVerification;
    /**
     * Create a new registration instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function postLogin(Request $request)
    {

        $this->validate($request, [
            'username' => 'required|string|max:255|min:1',
            'password' => 'required'
        ]);
        return view('auth.login');
    }

    public function register() {
        $scriptJs = 'register.js';
        $user = User::find(2);
        $from = 'antonm1.manzhos@gmail.com';
        //antonm1.mazhos@gmail.com

        //MAIL_DRIVER=smtp
        //MAIL_HOST=mintymint.net
        //MAIL_PORT=465
        //MAIL_USERNAME=hello@mintymint.net
        //MAIL_PASSWORD=123456789
        //MAIL_ENCRYPTION=tls


//        \Jrean\UserVerification\UserVerificationServiceProvider::sendQueue($user, $subject = null, $from = null, $name = null);
        $temp = \Jrean\UserVerification\Facades\UserVerification::sendQueue($user, $subject = null, $from, $name = null);
        $a = 10;
//        UserVerificationServiceProvider::sendQueue($user, $subject = null, $from = null, $name = null);
        return view('auth.register',compact('scriptJs'));
    }

    /**
     * Perform the registration.
     *
     * @param  Request   $request
     * @param  AppMailer $mailer
     * @return \Redirect
     */
    public function postRegister(Request $request)
    {
        $validatedData = $request->validate([
            'firstname' => 'required|string|max:255|min:1',
            'lastname' => 'required|string|max:255|min:1',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|regex:@[A-Z]@|regex:@[a-z]@|regex:@[0-9]@|regex:[\W]|confirmed',
            'password_confirmation' => 'required'
        ]);
        $user = User::create($request->all());
//        \Jrean\UserVerification\UserVerificationServiceProvider::sendQueue($user, $subject = null, $from = null, $name = null);
//        $request->request->add(['email_key' => $key = md5(microtime().rand())]);
//        UserVerificationServiceProvider::sendQueue($user, $subject = null, $from = null, $name = null);
//        UserVerification::sendQueue($user, $subject = null, $from = null, $name = null);
        $temp = 10;


        //$mailer->sendEmailConfirmationTo($user);
//        flash('Please confirm your email address.');
        return redirect()->back();
        //return view('auth.register');
    }
    /**
     * Confirm a user's email address.
     *
     * @param  string $token
     * @return mixed
     */
    public function confirmEmail($token)
    {
        User::whereToken($token)->firstOrFail()->confirmEmail();
        flash('You are now confirmed. Please login.');
        return redirect('login');
    }
}
